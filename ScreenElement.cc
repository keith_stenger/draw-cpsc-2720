#ifndef SCREENELEMENT_H
#define SCREENELEMENT_H
#include "Class.h"

#include<iostream>


class ScreenElement: public Screen
{
public:
   virtual void draw(Screen &screen)=0;
   virtual void read(istream &is)=0;
   virtual ~ScreenElement() {}

};

#endif
