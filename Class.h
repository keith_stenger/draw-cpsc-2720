#ifndef SCREENCLASS_H
#define SCREENCLASS_H

using namespace std;
class Screen
{
public:
 
  Screen(int, int);
  ~Screen();
  int get_rows();
  int get_col();
  void clear();
  void print();
  void set(int&, int&, char&);



private:
  int height, width;
  char **screen;
  char ch;


};
#endif
