CC=g++11
#CFLAGS are options that will be passed to the compiler
CFLAGS=-c -Wall -g -std=c++11

all: Draw

Draw: Draw.o Box.o Line.o Class.o ScreenElement.o Text.o TextBox.o
	$(CC) -o $@ $^

Draw.o: Draw.cc Box.h Line.h Class.h ScreenElement.h Text.h TextBox.h 
	$(CC) $(CFLAGS) -C $<

Box.o: Box.cc
	$(CC) $(CFLAGS) -C $<

Line.o: Line.cc
	$(CC) $(CFLAGS) -C $<

Class.o: Class.cc
	$(CC) $(CFLAGS) -C $<

ScreenElement.o: ScreenElement.cc
	$(CC) $(CFLAGS) -C $<

Text.o: Text.cc
	$(CC) $(CFLAGS) -C $<

TextBox.o: TextBox.cc
	$(CC) $(CFLAGS) -C $<

clean:	
	rm -f *.o

