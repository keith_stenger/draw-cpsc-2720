#include "Class.h"
#include <iostream>
#include<iomanip>

using namespace std;
//Constructor. 
Screen::Screen(int hei=24, int wid=80)
{
  height=hei;
  width=wid;
  
  //initializing dynamic memory
  screen= new char* [wid];

  for(int i=0; i<wid; i++)
    screen[i] = new char[hei];
  
   clear();
   print();
    
 
}

void Screen::clear()
{
  for(int w=0; w<width; w++)
    {
      for(int h=0; h<height; h++)
    	{
   	  screen[w][h]='1';
     	}
    }
  
}

void Screen::print()
 {
   int a,b;
   char c;
   for(int i=0; i<width; i++)
     {
       for(int j=0; j<height; j++)
 	{
 	  cout<<screen[i][j];
 	}
       cout<<endl;
     }
   cout<<get_rows()<<get_col();
   cout<<endl
       <<"enter in row col ch"<<endl;
    cin>>a>>b>>c;
    set(a,b,c);

 }

int Screen::get_rows()
{
  return width;
}

int Screen::get_col()
{
  return height;
}

void Screen::set(int& row, int& col, char& ch)
{
  
  
  screen[row][col]=ch;
   
  print();
}
//Destructor
Screen::~Screen()
{
   for(int i=0; i<width;i++)
   {
      delete [] screen[i];
   }
  delete [] screen;
  screen = nullptr; 
}

